# this file contains global variables and settings

locals {
  resume_bucket_name = "resume.isnor.ca"
  # for isnor.ca, created in advance, registered with Route53
  hosted_zone_id     = "Z1QGL3MVOZP94P"
  resume_domain_name = "resume.isnor.ca"
  # ID of the default managed "caching optimized" policy
  managed_cache_policy_id = "658327ea-f89d-4fab-a63d-7e88639e58f6" # Managed-CachingOptimized
}

# State configuration
terraform {
  backend "http" {
  }
}

# AWS configuration
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}