
resource "aws_acm_certificate" "resume_website" {
  domain_name               = "isnor.ca"
  subject_alternative_names = ["*.isnor.ca"]
  tags = {
    "Name" = "isnor.ca"
  }
}

resource "aws_route53_record" "resume_isnor_ca" {
  zone_id = local.hosted_zone_id
  name    = local.resume_domain_name
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.resume_cloudfront.domain_name
    zone_id                = aws_cloudfront_distribution.resume_cloudfront.hosted_zone_id
    evaluate_target_health = false
  }
}