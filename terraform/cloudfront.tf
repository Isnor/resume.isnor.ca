
locals {
  s3_origin_id = "resume.isnor.ca"
}

resource "aws_cloudfront_origin_access_identity" "resume_cloudfront" {
  comment = "Allow CloudFront to access the resume.isnor.ca bucket website"
}

resource "aws_cloudfront_distribution" "resume_cloudfront" {
  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Serve resume.isnor.ca"
  default_root_object = "index.html"
  price_class         = "PriceClass_100"

  aliases = [local.resume_domain_name]

  origin {
    domain_name = aws_s3_bucket.website_bucket.bucket_regional_domain_name
    origin_id   = local.s3_origin_id
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.resume_cloudfront.cloudfront_access_identity_path
    }
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id
    cache_policy_id  = local.managed_cache_policy_id

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
    compress               = true
  }

  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate.resume_website.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.2_2021"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}